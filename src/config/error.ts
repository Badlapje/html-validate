import { UserError } from "../error";

export class ConfigError extends UserError {}
